# Relazione dell'esercitazione 1 (Stefano Volpe, #969766)

## Parte 1

I comandi seguenti sono disponibili anche [nella mia _repository_ per questo
insegnamento](https://codeberg.org/foxy/unibo-93466-foundations-of-cybersecurity).
Per ogni livello `LEVEL`, mi sono prima connesso via ssh:

```bash
ssh bandit$LEVEL@bandit.labs.overthewire.org -p 2220
```

### Livello 1

Ho visualizzato il file `readme` che conteneva la soluzione:

```bash
cat readme
```

### Livello 2

Per evitare che il nome del file `-` venisse interpretato da `cat` come
prefisso di un'opzione, ho disambiguato specificando la cartella di riferimento
(`$HOME`, che è quella corrente):

```bash
cat ./-
```

### Livello 3

Per evitare che `bash` interpretasse ciascuna parola del nome del file come un
argomento diverso, ho usato le doppie virgolette:

```bash
cat "spaces in this filename"
```

### Livello 4

Per visualizzare anche i file nascosti, ho usato `-a`:

```bash
ls -a inhere/
cat inhere/.hidden
```

### Livello 5

Ho prima determinato il tipo dei vari file, e poi stampato l'unico ASCII:

```bash
file ./*
cat ./-file07
```

### Livello 6

Ho passato a `file` solo i file della giusta dimensione e non eseguibili, e poi
ho proceduto come nel precedente esercizio:

```bash
file $(find -size 1033c ! -executable)
cat ./inhere/maybeinhere07/.file2
```

### Livello 7

Ciascuna opzione filtra per una caratteristica diversa, e sto facendo partire la
ricerca dal percorso radice:

```bash
find / -user bandit7 -group bandit6 -size 33c
cat /var/lib/dpkg/info/bandit7.password
```

### Livello 8

Passo le line in uscita della stampa del file a `grep`, che le filtra cercando
solo la parola chiave in questione:

```bash
cat data.txt | grep millionth
```

### Livello 9

Prima ordino le righe, poi per ogni sequenza di righe tutte uguali rimuovo i
doppioni e le compatto usando il numero di copie come prefisso. Quindi le ordino
(il prefisso più basso finirà in cima) e ne prendo la prima.

```bash
sort data.txt | uniq -c | sort -r | head -n 1 -
```

### Livello 10

Dal file originale, stampo solo le sequenze di caratteri leggibili, e ricerco
`===`:

```bash
strings data.txt | grep ===
```

### Parte 2

Accendo la mia macchina virtuale su cui ho precedentemente installato Kali
Linux. Da Tor Browser, mi collego alla rete Tor e visito la pagina in questione.
Apro gli stumenti di sviluppatore con `Ctrl+Shift+C`, e osservo che nel sorgente
della pagina, e in particolare in un commento HTML, la bandiera viene mostrata
in chiaro: è `p4ssw0rd1`. Avrei quindi potuto anche trovarla direttamente
aprendo il sorgente della pagina con `Ctrl+U`.
