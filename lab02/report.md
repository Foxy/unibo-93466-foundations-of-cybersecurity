# Relazione dell'esercitazione 2 (Stefano Volpe, #969766)

## Esercizio 1

Lo scopo è recuperare il testo originale a partire da due _hash_ assumendo che:

- siano state generate con l'algoritmo MD5;
- i testi originali siano composti solo da lettere minuscole semplici;
- ciascuno dei testi originali sia lungo da 1 a 7 caratteri inclusi.

Procedo usando tabelle arcobaleno. Installo `rainbowcrack` come superutente:

```bash
# apt install rainbowcrack
```

Genero la tabella arcobaleno come superutente:

```bash
# rtgen md5 loweralpha 1 7 0 1000 100000 0
```

Ordino la tabella arcobaleno appena generata come superutente:

```bash
# rtsort /usr/share/rainbowcrack
```

Tento di recuperare il testo originale della prima _hash_:

```bash
$ rcrack /usr/share/rainbowcrack -h 6e6bc4e49dd477ebc98ef4046c067b5f
```

Il testo originale è "ciao".

## Esercizio 2

```bash
$ rcrack /usr/share/rainbowcrack -h e879410167dfb8670e483f7f7a1843cf
```

Il testo originale non è stato trovato. Riprovo generando una tabella arcobaleno
per ogni possibile lunghezza del testo originale e con catene più lunghe (10000)
in modo da avere più possibilità di riuscita. La generazione richiede più tempo,
ma a lunghezza 6 ho successo:

```bash
# rtgen md5 loweralpha 6 6 0 10000 100000 0
# rtsort /usr/share/rainbowcrack
$ rcrack /usr/share/rainbowcrack -h e879410167dfb8670e483f7f7a1843cf
```

Il testo originale è "zlsbga".

## Esercizio 3

Estraggo l'elenco di parole `rockyou.txt` come superutente:

```bash
# gunzip /usr/share/wordlists/rockyou.txt.gz
```

Uso `hashid` sulla _hash_ data per individuare l'algoritmo usato:

```bash
$ hashid bc107137cda7aa074de2664a88247f2dfa54546923049ec929869edd6bc648a0
```

Di tutte le possibili soluzioni resituite dal comando, parto da SHA-256, che è
molto diffusa. Mi serve la pagina di manuale di `hashcat` per capire quale
identificativo è assegnato dal comando a questo algoritmo:

```bash
$ man hashcat
```

Nel caso del sale usato come suffisso, è 1410. Procedo a scoprire la password
originale. Tento un attacco con il dizionario `rockyou.txt` specificando
_hash_ e sale dati dal testo:

```bash
$ hashcat -a 0 -m 1410 \
  bc107137cda7aa074de2664a88247f2dfa54546923049ec929869edd6bc648a0:dd1b1n5 \
  /usr/share/wordlists/rockyou.txt
```

Scopro che la _password_ era "qwerty".

## Esercizio 4

Uso `hashid` sulla _hash_ data per individuare l'algoritmo usato:

```bash
$ hashid 0e8ae09ae169926a26b031c18c01bafa
```

Di tutte le possibili soluzioni resituite dal comando, parto da MD5, che è
molto diffusa. L'indizio fa presumere che la _password_ non sia poi così
complessa, quindi applico un insieme di regole standard a `rockyou.txt`:

```bash
$ hashcat -a 0 -m 0 \
  -r /usr/share/hashcat/rules/InsidePro-PasswordsPro.rule \
  0e8ae09ae169926a26b031c18c01bafa \
  /usr/share/wordlists/rockyou.txt
```

Ottengo `ILOVEME8320`.

## Esercizio 5

Analogamente all'esercizio precedente:

```bash
$ hashid c73fceaab80035a75ba3fd415ecb2735
$ hashcat -a 0 -m 0 \
  -r /usr/share/hashcat/rules/InsidePro-PasswordsPro.rule \
  c73fceaab80035a75ba3fd415ecb2735 \
  /usr/share/wordlists/rockyou.txt
```

Ottengo `soccer23!`.
