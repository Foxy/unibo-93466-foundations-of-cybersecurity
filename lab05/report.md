# Relazione dell'esercitazione 5 (Stefano Volpe, #969766)

Installo le dipendenze di Ghida:

```sh
sudo apt update
sudo apt install default-jre default-jdk
```

Dopo aver scaricato Ghidra, lo apro:

```sh
./ghidraRun
```

Creo un nuovo progetto aggiungendo il file scaricato da Virtuale. So che devo
velocizzare il programma, e navigando fra il codice decompilato delle varie
funzioni osservo che `FUN_004011f6` invoca `usleep` con `in_EAX * 500000` come
parametro. Affinché `usleep` non abbia effetto, devo far sì che il suo parametro
venga valutato come 0. Cliccando su `500000` nel decompilato, identifico la
stessa costante nel disassemblato. È in esadecimale (`0x7a120`), e la
sostituisco con `0x0`, e cioè 0 sempre in esadecimale. Siccome non ho cambiato
la lunghezza dell'istruzione in questione, posso esportare il programma senza
problemi. Dopo averlo esportato, aggiungo i permessi di esecuzione:

```sh
chmod u+x fast_printer.file
```

Ora posso eseguire il file:

```sh
./fast_printer.file
```

E osservo che la bandiera è `tim3_flie5_WiTh_s0m3_heLp`.
